package fileProcessorDecorator.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileProcessor extends InputDetails {
	Scanner in;

	public FileProcessor() {

		String file = "input.txt";
		ArrayList<String> array = new ArrayList<String>();

		try {
			if (in == null) {
				in = new Scanner(new File(file));
			}

			BufferedReader br = new BufferedReader(new FileReader(file));
			try {
				if (br.readLine() == null) {
					System.out.println("No errors, and file empty");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			while (in.hasNext()) {
				String curr = in.nextLine();
				if (curr.isEmpty()) {
					continue;
				}

				if (curr.contains(".  ")) {
					String[] k = curr.split("(?<=[.]) \\s+");
					array.add(k[0]);
					description.add(k[0]);
					description.add(k[1]);
					array.add(k[1]);
					continue;
				}
				array.add(curr);
				description.add(curr);
			}

		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		}
	}
}
