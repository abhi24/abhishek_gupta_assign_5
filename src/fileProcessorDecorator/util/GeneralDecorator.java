package fileProcessorDecorator.util;

import java.util.ArrayList;

public abstract class GeneralDecorator extends InputDetails {
	public abstract ArrayList<String> getDescription();
}
