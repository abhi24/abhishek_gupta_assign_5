package fileProcessorDecorator.util;

import java.util.ArrayList;

public class ParagraphDecorator extends GeneralDecorator {

	private InputDetails inp;

	public ParagraphDecorator(InputDetails i) {
		inp = i;
	}

	@Override
	public ArrayList<String> getDescription() {
		return inp.getDescription();
	}

}
