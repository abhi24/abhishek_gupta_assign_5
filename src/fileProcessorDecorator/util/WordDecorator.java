package fileProcessorDecorator.util;

import java.util.ArrayList;

public class WordDecorator extends GeneralDecorator {

	public InputDetails inp;
	ArrayList<String> sentence = new ArrayList<String>();

	public WordDecorator(InputDetails i) {
		for (int j = 0; j < i.getDescription().size(); j++) {
			sentence.add(i.getDescription().get(j));
		}
		inp = i;
		inp.getDescription().clear();

		for (int j = 0; j < sentence.size(); j++) {

			if (sentence.get(j).contains(" ")) {
				String domain = sentence.get(j);
				String[] strArray = domain.split("\\s+");
				for (String str : strArray) {
					inp.getDescription().add(str);
				}
			} else {
				inp.getDescription().add(sentence.get(j));
			}
		}

	}

	@Override
	public ArrayList<String> getDescription() {
		return inp.getDescription();
	}

}
