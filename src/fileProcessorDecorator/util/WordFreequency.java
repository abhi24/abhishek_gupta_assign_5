package fileProcessorDecorator.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class WordFreequency {

	public InputDetails inp;
	ArrayList<String> sentence = new ArrayList<String>();
	// ArrayList<String> unique = new ArrayList<String>();
	int count = 0;

	public WordFreequency(InputDetails i) {
		for (int j = 0; j < i.getDescription().size(); j++) {
			sentence.add(i.getDescription().get(j));
		}
		inp = i;
		inp.getDescription().clear();

		Set<String> unique = new HashSet<String>(sentence);
		for (String key : unique) {
			// System.out.println(key + ":" + Collections.frequency(sentence,
			// key));
			inp.getDescription().add(key + ":" + Collections.frequency(sentence, key));
		}
	}

	public ArrayList<String> getDescription() {
		return inp.getDescription();
	}

}
