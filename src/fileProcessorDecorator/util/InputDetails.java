package fileProcessorDecorator.util;

import java.util.ArrayList;

public abstract class InputDetails {
	ArrayList<String> description = new ArrayList<String>();

	public ArrayList<String> getDescription() {
		return description;
	}
}
