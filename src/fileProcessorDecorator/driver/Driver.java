package fileProcessorDecorator.driver;

import fileProcessorDecorator.util.FileProcessor;
import fileProcessorDecorator.util.InputDetails;
import fileProcessorDecorator.util.ParagraphDecorator;
import fileProcessorDecorator.util.SentenceDecorator;
import fileProcessorDecorator.util.WordDecorator;
import fileProcessorDecorator.util.WordFreequency;

public class Driver {

	public static void main(String[] args) {
		System.out.println("hi");

		InputDetails ip = new FileProcessor();
		System.out.println(ip.getDescription().get(1));

		ParagraphDecorator p = new ParagraphDecorator(ip);

		System.out.println(p.getDescription());
		SentenceDecorator s = new SentenceDecorator(p);
		System.out.println(s.getDescription());
		WordDecorator w = new WordDecorator(s);
		System.out.println(w.getDescription());
		WordFreequency wf = new WordFreequency(w);
		System.out.println(wf.getDescription());

	}

}
